FROM ubuntu:16.04

ENV PROGRAMMABLE_CLOUD=0.1

RUN apt-get update &&\
    apt-get -y install  build-essential  supervisor wget &&\
    apt-get -y install  python uwsgi uwsgi-plugin-python nginx python-pip python-dev zlib1g-dev libjpeg-dev &&\ 
    groupadd pcloud &&\
    adduser --ingroup pcloud --shell /bin/false pcloud &&\
    install -v -o pcloud -g pcloud  -d /opt/functions &&\
    install -v -o pcloud -g pcloud  -d /opt/lib/core  &&\
    install -v -o pcloud -g pcloud  -d /opt/lib/cloud  &&\
    install -v -o pcloud -g pcloud  -d /opt/config    &&\
    install -v -o pcloud -g pcloud  -d /opt/templates   &&\
    install -v -o pcloud -g pcloud  -d /opt/tests   

## copy files
COPY functions/*.py  /opt/functions/
COPY lib/core/*.py   /opt/lib/core/
COPY lib/*.py  	      /opt/lib/
COPY lib/cloud/*.py   /opt/lib/cloud/
COPY config/*.yml /opt/config/       
COPY templates/*.yml /opt/templates/ 
COPY tests/*.py /opt/tests/
COPY api.py     /opt/         
## copy requirements
COPY requirements.txt /tmp/
RUN wget https://bootstrap.pypa.io/get-pip.py && python get-pip.py &&\
    ln -s /usr/local/bin/pip /usr/bin/pip && pip install -U pip &&\
    pip install -r /tmp/requirements.txt 
   
EXPOSE 5000    







