import asyncio



#async task server
def taskcallback(result):
    '''
      update Task result to redis queue
    '''
    pass

class  CreateTask():

    def __init__(self):
        self.loop = asyncio.get_event_loop()
        self.future = asyncio.Future()
        self.addcallback()
        self.tasks  = []

    def addTask(self,task,cb=None):
        self.tasks.append(asyncio.ensure_future(task()))
        
    def addcallback(self):
        '''
        '''
        self.future.add_done_callback(taskcallback)
         
    def getTask(self):
        '''
           readTask from redis queue and add to task queue  
        '''
        while True:
            yield from asyncio.sleep(1)
            task = gentask()
            print ("++Task++",task)
            self.addTask(task)

    def execTask(self):
        '''
            schedule Task execution
        '''
        pass 
  

test = CreateTask()

async def lazy():
    print ("i am mister lazy") 
    return 232
async def crazy():
    print ("iam mister crazy")

async  def runforever():
    while True:
        test.addTask(lazy)
        test.addTask(crazy)
        await asyncio.sleep(1)

test.loop.run_until_complete(runforever())
test.loop.close()







