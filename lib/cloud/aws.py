from cloud import cloud
import subprocess
import json

class cloudService(cloud):

    def __init__(self,name,template=None,**kargs):
    #def __init__(self):
        '''
          add init variables here for cloud services
        '''
        #self.region            = kargs['region']
        #self.availabiltyzone   = kargs['availabiltyzone']
        #self.template          = template
        self.cloud              = name
        self.state              = None
          
    @classmethod
    def makecloudservice(cls,name,**kargs):
       return cls(name,**kargs)

    def execute(self, cmd):
        try:
            print "\nexecuting %s" % cmd
            output = subprocess.check_output(cmd, shell=True)
            return output
        except subprocess.CalledProcessError as e:
            return False

    def _createAWSInstance(self,imageid=None,subnetid=None,securitygroupid=None,profile=None,instancename=None,keyname=None):
        """
           Internal Method to create AWS instance
           Minimum required arguments are:
           --image-id
           --subnet-id
           --security-group-id
           --key-name
           --tag-specifications
        """
        ec2_launch_cmd = 'aws ec2 run-instances --image-id %s --count 1 --instance-type t1.micro --subnet-id %s \
--security-group-id %s --associate-public-ip-address --key-name %s '\
%(imageid, subnetid,securitygroupid, keyname)

        print ec2_launch_cmd
       
        op = self.execute(ec2_launch_cmd)

        try:
            op = json.loads(op)
            instance_id = op['Instances'][0]['InstanceId']
            self.state   = 'pending'
            return instance_id,instancename,
        except ValueError as e:
            print e
		
    def check_ec2_running_status(self,instance_id=None):
        print "checking if EC2 instance is in running state or not"
        cmd = 'aws ec2 describe-instances --instance-ids %s' %(instance_id)
        op = self.execute(cmd)
        print op
        
        if 'running' in op:
            print "System is running and we can proceed with the tests"
            self.state = 'running'
        else:
            print "Waited for 300 secs still couldn't find the state change to running thus raising exception"
            raise Exception("Raising Exception")

    def createInstance(self,**kargs):
       '''
          create Instance on requested Cloud Service 
          try to use a template to create an instance 
       '''
       if self.cloud == 'aws':
          ### process kargs	
          print "creating cloud instance"
 
          op = self._createAWSInstance(**kargs)
          print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++>",op
          return op
          #self._createAWSInstance(**kargs)
          ## create checkstatus message with callback function
          ## produceStatus
       
    def deleteInstance(self,instance_id=None):
        print "deleting/terminating ec2 instance"
        cmd = 'aws ec2 terminate-instances --instance-ids %s' %(instance_id)
        op = self.execute(cmd)
        self.state = 'stopping'

        print (op)

        if "shutting-down" in op or "terminated" in op:
            print "Instance is getting terminated"
            self.state = 'terminated'
        else:
            print "Termiantion failed check output"
            raise ('Termination failed')

    def modifyInstance(self):
       pass 

    def updateInstance(self):
       pass

    def servicesrequested(self):
       pass


def getCloudService(name,template=None,**kargs):
       svc =  cloudService.makecloudservice('name',**kargs)
       return svc
   


if __name__ == '__main__':
    c = getCloudService('aws')
    c._createAWSInstance(imageid='ami-0f4cfd64',instancename='sample-test')
    c.deleteInstance(instance_id='i-00df202d2dbce56a8')
