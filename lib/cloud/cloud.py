import abc


class cloud:
   '''
     Base cloud class interactes with AWS/GCP/AZURE clouds.  this class exposes programmbale fuction 
     for all cloud services, A function create instances to the class for example if a user want to deploy 
     instances on google and AWS cloud with Basic scanning and configuration 
   '''	
   __metaclass__ = abc.ABCMeta

   '''
     These method must be implemented
   '''
   @abc.abstractmethod
   def createInstance(self):
       raise NotImplemetedError("Not implemented")
       
   @abc.abstractmethod
   def deleteInstance(self):
       raise NotImplemetedError("Not implemented")

   @abc.abstractmethod
   def modifyInstance(self):
       raise NotImplemetedError("Not implemented")
   
   @abc.abstractmethod 
   def updateInstance(self):
       raise NotImplemetedError("Not implemented")
       
   @abc.abstractmethod       
   def servicesrequested(self):
       raise NotImplemetedError("Not implemented")
   '''
    define class properties here
   '''
   @property
   def show(self):
        return self.cloud
 

      

        
