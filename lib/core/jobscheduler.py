import tasks
import redis
from worker import worker,workers
from funfactory import servfuns
import Queue

# this should come from config
WORKERS=1

class Job:

    @classmethod
    def createJobrequest(cls,funcname=None,**kargs):
        '''
            job constructor
        '''
        return cls(funcname,**kargs)

    def __init__(self,funcname,**kargs):
        self.params         = {}
        self.params['func_name']  = funcname
        self.params['func_args']  = kargs

class jobscheduler(workers):

    def __init__(self,function_class=None,connection=None):
        self.connection = connection
        self.funs       = function_class
        self.workers = []
        self.jobid   = 7000
        self._initialize()
    
    def getJobid(self):
        self.jobid+=1  # replace this with uuid
        return self.jobid
    
    def _initialize(self):
        ## lazy start
        for i in range(WORKERS):
           self.worker= worker(function_class=self.funs,connection=self.connection)
           self.worker.start()
           print "registering ", self.worker,self.worker.state
           self.workers.append(self.worker)

    def getavailableworker(self):
        # change this func later 
        while True:
            for worker in self.workers:
                if worker.state == 'AVAILABLE':
                   return worker
        
    def notifyworkers(self,taskid):
            ## TO DO check worker state
            worker = self.getavailableworker()
            print "I am notify the next available worked for task"
            workers.notifyworker(worker,taskid,self.connection)

    def enqueueJobs(self,func=None,**kargs):
        '''
          flask invokes this function 
          to eneque jobs to the backend 
        '''
        job      = Job.createJobrequest(funcname=func,**kargs)
	task     = tasks.task.create(queue='TASK',redis_connection=self.connection)
        taskmsg  = task.buildTask(taskid=self.getJobid(),status='PENDING',**job.params)
        taskid   =  task.setTask(taskmsg)
	task.enqueueTask(self.connection,'TASKS',taskmsg)
        #self.notifyworkers(taskid)
        return taskid

    def getJobstatus(self,taskid):
        task = tasks.task.getTask(self.connection,taskid)
        if task['status'] == 'DONE':
            if task['result'] is not None:
                return task['result']
        return False  

def init():
    connection = redis.StrictRedis(host='localhost')
    funs  = servfuns()
    job   = jobscheduler(function_class=funs,connection=connection)
    return job

job = init()
# def _createAWSInstance(self,imageid=None,subnetid=None,securitygroupid=None,profile=None,instancename=None,keyname=None):
#args = {'imageid':'ami-028d8978','subnetid':'subnet-b37161d7','securitygroupid':'sg-08fdb37e','keyname':'sandeepTest','cb':'checkInstanceStatus'}
#job.enqueueJobs(func='createInstance_fun',**args)


