import callable_pip
import importlib
#from pip._internal import main as pipmain

def buildpackages(*packages):
    for package in packages:
        try:
            importlib.import_module(package)
        except ImportError:
            pass
            callable_pip.main('install',package,'-q','--user','--ignore-installed')  
            #pip.main(['install',package,'--ignore-installed'])
            ### this is a hack ,the pillow install packages as  PIL  
            if package == 'Pillow': package = 'PIL'    
            globals()[package] = importlib.import_module(package)
            
    return        
