import yaml
import os


class ProcessConfig:
    '''
      function configuration class
    '''
  
    def __init__(self,configpath):
       
        self.config     = None
        self.packages   = []
        self.functions  = {}
        self.buildconfig(configpath)
        self.getpackagetobeimported()

    def loadconfig(self,config_file):
        with open(config_file) as ymlfile:
            cfg = yaml.load(ymlfile)
        return cfg    

    def buildconfig(self,path):
        files = map(lambda x:os.path.join(path,x), os.listdir(path))
        cfg   = map(self.loadconfig,files)
        #self.config = cfg
        self.config =  (list(cfg))

    def getpackagetobeimported(self):
        for item in self.config:
            for k in item.keys():
                if 'packages' in  item[k].keys():
                    packages = [ x for x in  item[k]['packages']]
                    for pack in packages: self.packages.append(pack)

    def functiondesc(self):
        pass

    def funcparams(self):
        pass

    def funcoutput(self):
        pass

    @property
    def getconfig(self):
       # print (list(self.config))
        return self.config
 
    @property
    def getpackages(self):
        return self.packages


     
    


