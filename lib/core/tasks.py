import redis
from   lib.core.message import Message
try:
 import cPickle as pickle
except ImportError:
 import pickle

class task:
    """
     the class manages redis queue
     we use redis queue to enqueue and process jobs. Since many of backend jobs has long IO 
     we can delegate this job to backgroud processes.
    """
    @classmethod
    def create(cls,queue=None,redis_connection=None):
        task = cls(redis_connection,queue)
        return task

    def __init__(self,redis_connection,queue):
        '''
           redis job 
           fun_name=<>,args=<>,timout=<>
        '''
        self.redis_connection = redis_connection
        self.queue            = queue
        self.taskid           = 1000 
    

    def buildTask(self,taskid=None,timeout=None,msgclass=None,cb=None,taskType=None,**taskargs):
        self.Msg = Message()
        self.Msg.MsgType    = taskType
        self.Msg.MsgClass   = msgclass
        self.Msg.MsgTimeout = timeout
        self.Msg.msgID      = taskid
        self.Msg.cb         = cb
	#self.Msg.status     = status
        self.Msg.body       = taskargs
        return self.Msg
    
    def setTask(self,taskmsg):
        taskid    = taskmsg.msgID
        taskargs  = taskmsg.body
        self.redis_connection.hmset(taskid, taskargs)
        return taskid

    @staticmethod 
    def getTask(connection,taskid):
        return connection.hgetall(taskid)  
 
    @staticmethod 
    def updateTask(connection,taskid,jobObj):
        connection.hmset(taskid, jobObj)

    @staticmethod
    def enqueueTask(connection,queue,Taskmsg,pyobj=True):		
        '''
         enqueue the task to respective queue	
        '''
        if pyobj:
           Taskmsg = pickle.dumps(Taskmsg)
        connection.rpush(queue,Taskmsg)

    @staticmethod 
    def dequeueTask(connection,queue,pyobj=True):
        '''
        POP up task from respective queue		
        '''
        ret = connection.rpop(queue)
        if pyobj and ret is not None:
            ret =  pickle.loads(ret)  
        return ret 



#connection =  redis.StrictRedis(host='localhost')
#job = task.create(queue='TASK',redis_connection=connection)
#taskmsg = job.buildTask(status='PENDING',taskid=4,func_name='piarea',func_args=2)
#taskid  = job.setTask(taskmsg)
#print job.getTask(connection,taskid)
#job.enqueueTask(connection,'TASKS',taskmsg)
#ret = job.dequeueTask(connection,'TASKS')
#ret = pickle.loads(ret)
#print ret.body


