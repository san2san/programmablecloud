import time
from threading  import Thread
from funfactory import loadfuns
from tasks	import task
from ast import literal_eval
from message import Message

class  workers:
    ''' 
       workers class
    '''
    def __init__(self):
        self.workers = []
    
    def register_workers(self,worker):
        '''
           register all the workers, check work state 
           and register them for JOB. 

        '''
    @staticmethod
    def notifyworker(worker,taskid,connection=None):
        ### read the redis queue and notify worker
        print "notifying",taskid
        jobobj =  tasks.task.getTask(connection,taskid)
        funcname = jobobj['func_name']
        funcargs = jobobj['func_args']
        print "executing the func  {} with params {}".format(funcname,funcargs)
        worker.execfunction(jobobj)
        print "notification done"

###########################################                              

class worker(Thread):
    def __init__(self,function_class=None,connection=None):
        Thread.__init__(self)
        self.state = 'AVAILABLE'
	self.queue = 'TASKS'        
        self.connection = connection
        self.funs = function_class
        self.stop = True
        self.execfun = False
        self.params  = None
        self.job  = None
        self.DECODE = False
	self.cbtask  = task.create(queue='ASYNC',redis_connection=self.connection)
        
    def execfunction(self,job):
        if self.execfun:
            print "Already running func try later"
        self.job = job
        self.execfun = True
        self.state   = 'RUNNING'

    def stopworker(self):
        pass

    def decodefuncargs(self,fun):
        self.args =  self.funs.func_input(fun)
        for arg,paramKey in zip(self.args,self.params.keys()):
             print arg,self.params[paramKey]
             # To do input verification

    def kill(self):
        '''
            kill the thread, remmeber the job 
            may go into unknown state. makesure
            job status timesout or go into availble state.
        '''
        self.stop = False

    def cacheresult(self,taskid,msgBody):
        '''
           cache result in queue since thread execute asynchronosly 
           store results in redis cache

        '''
        task.updateTask(self.connection,taskid,msgBody)

    def sendTaskcbMsg(self,taskmsg):
        task.enqueueTask(self.connection,'ASYNC',taskmsg)
 
    def execFunction(self,msg):
         
        self.function = msg.body['func_name']
        self.params   = msg.body['func_args']
        self.status   = msg.body['status']  
        if isinstance(self.params,str):
                self.params = literal_eval(self.params)
                  #self.decodefuncargs(self.function)
        if 'cb' in self.params:
	    cb_fun =  self.params.pop('cb')
        ret =  self.funs.execfunc(self.function,self.params)
        print "Executed =====> {} with param {} ret is {}".format(self.function,self.params,ret)
        if cb_fun:
            print "Found callback Schedulling exec"
	    fun =  cb_fun
            msg.body['status'] = 'CB_PENDING'
            msg.body['result'] =  None
            cbargs  = {}
            cbargs['cb_args'] = ret
            #msg.body['cb_args'] = {'arg':ret}
            taskmsg =  self.cbtask.buildTask(taskid=msg.msgID,cb=fun,taskType='cb',**cbargs)
            self.sendTaskcbMsg(taskmsg)
            self.cacheresult(msg.msgID,msg.body)
            return
        msg.body['status'] = 'DONE'
        msg.body['result'] =  ret 
        self.cacheresult(msg.msgID,msg.body)
        self.execfun = False 
        self.state = 'AVAILABLE'  

    def run(self):
          while self.stop:
                  time.sleep(2)
                  print "Waking up to munch my lunch" 
                  msg = task.dequeueTask(self.connection,self.queue)
                  if isinstance(msg,Message):
			self.execFunction(msg)
                    

