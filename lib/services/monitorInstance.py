"""
   This service monitor instance health, it could be deployed as a sidecar, collecting instance
   statistics or could be deployed as a service within the instance. 
"""
