import json
import argparse
import requests

URL='http://0.0.0.0:5000/'
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

functions = []


def send(function,jsonpayload):
         
	posturl = "{}{}".format(URL,function)
        ret = requests.post(posturl,json=jsonpayload,headers=headers)
	return ret.text

def fetchresult(taskid):
	
	payload = {'jobid': taskid}
	geturl = "{}{}/{}".format(URL,'jobstatus',taskid)
        ret = requests.get(geturl)
        return ret.text

if __name__=="__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument("-function", help="function name")
	parser.add_argument("-jsonfile", help="json file containing function args")
	args = parser.parse_args()
	with open(args.jsonfile) as fin:
	     jsonpayload = json.loads(fin.read())
  	send(args.function,jsonpayload)
