import json
import sys
from pyclient import send,fetchresult
import time


def Test(function,args):
	'''
	   run Tests:
	'''
        taskid = send(function,args)
        print "received Task id as {}".format(taskid)
	return int(taskid)

def runTests(funs):
    #functions
    # radius function
    ret = {}
    for k,v in funs.iteritems():
        fun=k
        fun_args={}
        ret[fun] = {}
        for arg,val in v.iteritems():  	
              if arg =='ret':
 		  ret[fun].update({'ret':val})
                  continue
              fun_args[arg] = val
        taskid = Test(fun,fun_args)  
        ret[fun].update({'taskid':taskid}) 
    return ret  

def procresults(tasks):
    flag = True
    while flag:
         flag = False
   	 for key,val in tasks.iteritems():
        	taskid = val['taskid']
                result = val['ret']
       		ret = json.loads(fetchresult(taskid))
                if ret == 'PENDING':
		   flag = True 	
                else:
                   print "checking for taskid",taskid
                   assert int(result) == int(ret)    
                   print "CHECK PASSED"  
         time.sleep(3)   

if __name__=="__main__":
   ## prepare functions
   function = {}
   function['piarea'] = {'radius':12,'cb':'testcb','ret':72}
   #function['sqarea'] = {'side':4 , 'ret':16} 
   ret = runTests(function)
   procresults(ret)
   

